# Notes
Many thanks to the internet guides, expecially [Brian Connelly](http://bconnelly.net/working-with-csvs-on-the-command-line/)

# Simple stuff

 - pwd - Print working directory
 - cd - goto
 - ls - list files in the directory
 - mkdir - new folder
 - rmdir - remove folder
 - cp,mv,rm - copy, move, delete
 - man - integrated manual/help
 - history - history of terminal commands
 - top - system summary info
 - [grep](##grep) - grep: finds lines matching (or not matching) a given pattern ([main page](http://www.linuxmanpages.com/man1/grep.1.php)) (ls | grep searchedname)
 - [find](##find) - finds exact matches
 - [touch](##touch) - creates empty files
 - [curl](##curl) - downloads files
 - [diff](##diff) - visual difference between file contents
 - [zip](##zip) - archive tool
 - [scp](##scp) - secure copy of files from remote to local
 - [wget](##wget) - download files
 - [head](##head) - print a sample of the data file in terminal ([main page](http://www.linuxmanpages.com/man1/head.1.php))
 - [sed](##sed) - prints file content, perform simple manipulations to text in a file ([main page](http://www.linuxmanpages.com/man1/sed.1.php))
 - [awk](##awk): AWK is a programming language (and command) that is great for processing text ([main page](http://www.linuxmanpages.com/man1/awk.1.php))
 - [cat](##cat): concatenates and prints files ([main page](http://www.linuxmanpages.com/man1/cat.1.php))
 - [cut](##cut): removes specific bytes, characters, or fields from files ([main page](http://www.linuxmanpages.com/man1/cut.1.php))
 - [tail](##tail): outputs the end of a file ([main page](http://www.linuxmanpages.com/man1/tail.1.php))
 - tr: changes (translates) characters within a file ([main page](http://www.linuxmanpages.com/man1/tr.1.php))
 - wc: counts the number of characters, words, and lines in a file ([main page](http://www.linuxmanpages.com/man1/wc.1.php))
 - [iconv](##iconv): converts encoding of the file
 - [tr] (##tr): tr utility
 - [FFMpeg] (##FFMpeg): Video scripts
 - [Pandoc] (##Pandoc); Pandoc

# General info

## Use spaces in filename
```
cd ~/Library/Application\ Support
```

# More info

## sed

### Replace characters in the text files
 - ```s``` is used to replace the found expression "foo" with "bar"
 - ```g``` stands for "global", meaning to do this for the whole line. If you leave off the g and "foo" appears twice on the same line, only the first "foo" is changed to "bar".
 - ```-i``` option is used to edit in place on filename.
 - ```-e``` option indicates a command to run.
```
sed -i -e 's/foo/bar/g' filename
```

### Remove trailing comma from the end of CSV file
```
sed 's/, *$//' /Users/martin/Downloads/Reconcile/CSV/fixed/p01-2017-08-31.csv > p01-2017-08-31_fix.csv
```

### Delete/Replace a Carriage Return (CR) with sed
```
sed 's/.$/<replace with text>/'
sed 's/\x0D$/<replace with text>/'
sed 's/\r/<replace with text>/'
```

### Delete/Replace a Linefeed (LF) with sed
```
sed ':a;N;$!ba;s/\n/<text>/g'
```
where:   
 - :a       - create a label 'a'
 - N        - append the next line to the pattern space
 - $!       - if not the last line
 - ba       - branch (go to) label 'a'
 - s        - substitute
 - /\n/     - regex for new line
 - /<text>/ - with text "<text>"
 - g        - global match (as many times as it can)

### print a row span (200-220) from CSV (into separate file problems.csv)
```
sed -n '200,210p' Contacts_New.csv > problems.csv
```

## grep

### Search recursively in particular file types
```
grep -Hrno 'pagination' * --include \*.json
```
### Search recursively excluding some particular file types
```
grep -Hrno 'pagination' * --exclude \*.html
```

## cat

### Merge CSVs  
 - with headers  
```
cat file1.csv file2.csv file3.csv > bigfile.csv
```   
 - without headers  
```
cat < file1.csv <(tail +2 file2.csv) <(tail +2 file3.csv) > bigfile.csv
```  
or   
```
cat input1.csv > combined.csv
cat input2.csv | sed "1 d" >> combined.csv
cat input3.csv | sed "1 d" >> combined.csv
```

## Cat and Grep together

### Filter text-lines of output
```
cat /var/log/apache2/*.log | grep "shutting down"
```

## less

### Previews content of large file
```
less shoppingList.txt
```

## tail

### End of the file
```
tail /var/log/apache2/error.log
```

## awk

### count columns in CSV file
```   
awk '{n+=1} END {print n}' filename.csv
```   

## ack

### Search recursively
```
ack -i Authors *
```

### Search non-recursively
```
ack -n Authors *
```

### find all HTML files in the source tree
```
ack -f --html Authors
```
_The -f option  only prints the files that would be searched without actually doing any searching. The –html option is a special feature of ack. Ack understands many file types, and by specifying this option, you ask it to only search for HTML files._  

### Search all JavaScript files, case-insensitively, for the word “Authors”.
```
ack -f --html Authors
```
_The –js option tells ack to only search in JavaScript files. You can search for all kinds of other file types, e.g. –php, –python, –perl, et cetera._

### Exclude filetypes in the search
```
ack -w --type=nojs Authors
```

## git

### Terminal command to show .gitignore within folder
```
chflags nohidden .gitignore
```
or
```
chflags nohidden /path/to/dir/.gitignore
```
### Create .gitignore file
```
touch ~/.gitignore
```

### Open all files in Visual Studio Code (ignoring .gitignore files)
```
vscode `ls | grep -vx -f .gitignore`
```
config:
_if you edited .bash config file:_ (zsh)
```
nano ~/.zshrc
```
or (standard bash)
```
nano ~/.bashrc
```
code to place there:
```  

## visual studio code

##code () { VSCODE_CWD="$PWD" open -n -b "com.microsoft.VSCode" --args $* ;}
```
Visual Studio Code will prompt you and install itself the correct ENV PATH

## touch

### create empty files
```
touch .gitignore
```
```
touch one.txt two.txt three.txt
```

## curl

CURL is a great tool for making requests to servers; especially, I feel it is great to use for testing APIs.
To upload files with CURL, many people make mistakes that thinking to use -X POST as regular form data; in facts, that way will cause errors.

### Download files via terminal
```
curl -O http://www.domain.com/path/to/download.tar.gz
```

### Upload files with CURL
```
$ curl -F ‘data=@path/to/local/file’ UPLOAD_ADDRESS
```

For example, if I want to upload a file at /home/petehouston/hello.txt to the server http://localhost/upload which processes file input with form parameter named img_avatar, I will make request like this,
```
$ curl -F 'img_avatar=@/home/petehouston/hello.txt' http://localhost/upload
```

Upload multiple files
To send upload request for multiple files, just add an additional -F option,
```
$ curl -F 'fileX=@/path/to/fileX' -F 'fileY=@/path/to/fileY' ... http://localhost/upload
```

Upload an array of file
To send upload request for an array file, simply put additional -F options with same form parameter name as array,
```
$ curl -F 'files[]=@/path/to/fileX' -F 'files[]=@/path/to/fileY' ... http://localhost/upload
```

## diff

### shows difference in the content of two files
```
diff originalFile newFile
```
### Save diff output into the file
```
diff originalFile newFile > changes.patch
```

## find

### Find all directories with the “logs” name in the /var/www directory using the -type option:
```
find /var/www -type d -name logs
```
### To search PHP files in the current directory, add the -name option:
```
find . -type f -name "*.php"
```
### Find files with defined permissions using the -perm option:
```
find . -type f -perm 0777 -print
```
### Find all files which are greater than 500MB:
```
find / -size +500M
```

## zip

### Create archive
```
zip archive-name.zip directory-or-file-name
```

### See archive content
```
unzip -l archive-name.zip
```

### Unarchive
```
unzip archive-name.zip
```

## tr
tr: A simpler text manipulation tool

### Replace a Newline (CRLF)
```
tr '\r\n' '<replace with text>' < input_file > output_file
```

### Delete a Newline (CRLF)
```
tr -d '\r\n' < input_file > output_file
```

## scp

### dowload files from remote to local
```
scp /path/to/local/file username@hostname:/path/to/copy/to/
```
or
```
scp user@server:/path/to/remotefile.zip /Local/Target/Destination
```
handling spaces in filenames:
```
scp osxdaily@192.168.0.45:"/some remote directory/filename.zip" ~/Desktop/
```
multiple files
```
scp user@host:/remote/path/\{file1.zip,file2.zip\} /Local/Path/
```

## wget

### Download directory
```
wget -r ftp://username:password@1.2.3.4/dir/*
```

### Download file
```
wget ftp://username:password@1.2.3.4/dir/file.csv
```

### Ask password
```
wget --user=vivek --ask-password http://192.168.1.10/docs/foo.pdf
```

### Limit attempts
```
wget --tries=2 --user=vivek --ask-password http://192.168.1.10/docs/foo.pdf
```

### Read username and password from a file?

This is a security feature. Create a ~/.wgetrc file. The syntax is as follows to store login credentials:
```
$ cat ~/.wgetrc
user=vivek
password=myCoolPasswordhere
```
Of course, you can configure separate FTP and HTTP credentials as follows too:
```
$ cat ~/.wgetrc
ftp_user=vivekftp
ftp_password=myCoolFTPPassword
http_user=vivekhttp
http_password=myCoolHTTPPassword
```
Make sure only you can read the ~/.wgetrc file:
```
$ chmod 0400 ~/.wgetrc
$ chown vivek:vivek ~/.wgetrc
```

## head

### display n rows
```
head -n6 Contacts_New.csv
```

### display c bits from the beginning (chars)
```
head -c100 Contacts_New.csv
```
or long number: (k for 1024x, m for 1048576)
```
head -c100k Contacts_New.csv
```
### Example

It could also be piped to one or more filters for additional processing. For example, the sort filter could be used with its -r option to sort the output in reverse alphabetic order prior to appending file1:   
```
ls | head | sort -r >> file1
```

## iconv
```
iconv -f ascii -t utf-8 Contacts_New.csv > converted.csv
```
 - iso-8859-1
 - utf-8
 - ascii
 - US-ASCII

## CSV

### File info
```
file -I <filename>
```

### Extract CSV columns
```
cut -d , -f 2,4-6 input.csv
```

### Encoding conversion
```
uconv --from-code ISO_8859-1 --to-code UTF8
```
or use `iconv`

### Strip non-printable ASCII characters
```
perl -pi.bak -e 's/[\000-\007\013-\037\177-\377]//g;'
```

### Extract columns based on values
first one creates a file with header only, the next one concatenate that with the output   
```
head -n 1 input.csv > pizza.csv
grep "pizza" input.csv >> pizza.csv
```

Compare two CSV files
```
$ awk -f script.awk file1 file2
736493, Match
763498, In file1 but not in file2
983264, In file1 but not in file2
```

### Combining Columns from Multiple CSVs
If the fields of your data are spread across multiple files, these can be combined (horizontally) using paste:   
```
paste -d , input1.csv input2.csv > combined.csv
```

### Determining the Number of Columns
```
cat input.csv | grep -v "^#" | awk "{print NF}" FS=,
```
### Determining the Number of Unique Columns
```
cat input.csv | grep -v "^#" | awk "{print NF}" FS=, | uniq
```

### Convert XLSX into CSV
```
xlsx2csv -l '\r\n' -e /Users/martin/Downloads/Reconcile/XLS/p06-2017-10-04.xlsx > p06-2017-10-04.csv
```

### Add date column in CSV file
```
mydate=$(date)
awk -v d="$mydate" -F"," 'BEGIN { OFS = "," } {$6=d; print}' input.csv > output.csv
```

## Base64

### To encode text to base64, use the following syntax:
```
$ echo -n 'scottlinux.com rocks' | base64
c2NvdHRsaW51eC5jb20gcm9ja3MK
```

### To decode, use base64 -d. To decode base64, use a syntax like the following:
```
$ echo -n c2NvdHRsaW51eC5jb20gcm9ja3MK | base64 -D
scottlinux.com rocks
```

## FFMpeg

### Rotate Video
```
ffmpeg -i thatvideo.mov -vf "transpose=2" ~/Desktop/thatvideo.mov
```

## Pandoc

### Convert Markdown into a Word document
```
pandoc -o output.docx -f markdown -t docx filename.md
```

### Convert doc into Markdown
```
pandoc -f docx -t markdown foo.docx -o foo.markdown
```

### Convert doc into Markdown (with extracting images)
```
pandoc -s FILE.docx --extract-media=FOLDER -t markdown -o FILE.md
'''

### Convert Markdown into HTML document
```
pandoc -f html -t markdown -o test.md test.html
```

### Pandoc Supported Formats
```
-t gfm (GitHub-Flavored Markdown)  
-t markdown_mmd (MultiMarkdown)  
-t markdown (pandoc’s extended Markdown)  
-t markdown_strict (original unextended Markdown)  
-t markdown_phpextra (PHP Markdown Extra)  
-t commonmark (CommonMark Markdown)  
-t docx (docx)  
-t html (HTML)  
-t latex (LaTeX)  
-t mediawiki (MediaWiki markup)  
-t textile (Textile)  
-t rst (reStructuredText)  
-t docbook (DocBook)  
-t t2t (txt2tags)  
-t odt (ODT)  
-t epub (EPUB)  
-t opml (OPML)  
-t org (Emacs Org mode)  
-t twiki (TWiki markup)  
-t haddock (Haddock markup)
```

### A4 or US Letter

pandoc default behaviour is to output to US Letter paper size - which made my local printing shop raise their eyebrows.

```
pandoc -s -V geometry:a4paper -o outfile.pdf infile.md
```
